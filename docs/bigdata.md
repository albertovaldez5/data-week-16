# Big Data

Big data refers to all data that exceed operational databases or that doesn&rsquo;t fit in a local machine. It has four characteristics:

1.  Volume: Large amounts of data, terabytes, years worth of transactions.
2.  Velocity: How quickly data comes in, world wide customer purchases.
3.  Variety: Different forms of data, product details, user info, metadata, etc.
4.  Value: We want to know what we want to get from the data, specificity.
5.  Veracity: Uncertainty of data, questionable sources, automated processes, bots, etc.


## Big Data Technologies

Apache Hadoop (Hadoop)<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup> is one of the most popular open source frameworks, with numerous technologies for big data. Google developed Hadoop to process large amounts of data by splitting data across a distributed file system.

1.  Hadoop Distributed File System (HDFS) is a file system used to store data across server clusters (groups of computers). It is scalable (which means it handles influxes of data), fault-tolerant (handles hardware failure), and distributed (spread across multiple servers connected by a common core).
2.  MapReduce is a programming model and processing technique for big data. MapReduce enables processing the large amount of data spread across the cluster in the HDFS by performing the same task for each file system.
3.  Yet Another Resource Negotiator (YARN) manages and allocates resources across the clusters and assigns tasks.


## MRJob library

Mrjob provides an interface for map-reduce process in a dataset. It works by creating a class that inherits from `MRJob` and parsing input file from the stdin. In this case we use input.txt right after calling the script.

```python
from mrjob.job import MRJob

class Bacon_count(MRJob):
    def mapper(self, _, line):
        for word in line.split():
            if word.lower() == "bacon":
                yield "bacon", 1

    def reducer(self, key, values):
        yield key, sum(values)

if __name__ == "__main__":
   Bacon_count.run()
```

```shell
python bacon_counter.py input.txt
```

    "bacon"	21


## Hadoop

Hadoop is a brand that refers to large scale processes, it is only a storage technology. Spark can run on top of Hadoop or use an alternative storage source.

Hadoop collects data and distributes it, it will index the data using hash technology, partition it in different hardware drives and then whenever it is needed, it will access it via hash.

Hadoop also uses redundancy for ensuring the data keeps running if a cluster fails.

Hadoop is disk intensive, as it stores everything in the drive, while Spark stores everything in memory which makes it faster. However, memory is way more expensive than disks, so we must have a balance. When running in the cloud, Spark can use the memory of multiple machines.


# Spark

Spark is an engine and environment that will give us access to resources to run our scripts a way to process large scale data. It enables our programming languages in a huge scale.

Spark can be installed in a local computer, a network of computers, run it on a Hadoop system or in the cloud.

Spark doesn&rsquo;t read data sequentially, but rather it breaks it and reads them in different pieces and then rebuilding the complete file in the back. It also uses lazy evaluation, so it waits until an action is required to run all the instructions.


## Start Spark

```python
from pyspark.sql import SparkSession

spark = SparkSession.builder.appName("DataFrameFunctions").getOrCreate()
```

We are going to add the file to the Spark context. In case of using the cloud, it will be downloaded to the cloud&rsquo;s storage.

Then we are going to read the data using `spark.read` and the file format, then access the file via `SparkFiles.get()`.

```python
from pyspark import SparkFiles


url = "../resources/wine.csv"
spark.sparkContext.addFile(url)
df = spark.read.csv(SparkFiles.get("wine.csv"), sep=",", header=True)
```

We can access the `df` dataframe object now. These dataframes are database tables, so we can access their schema via `printSchema()`.

```python
df.printSchema()
```

    root
     |-- country: string (nullable = true)
     |-- description: string (nullable = true)
     |-- designation: string (nullable = true)
     |-- points: string (nullable = true)
     |-- price: string (nullable = true)
     |-- province: string (nullable = true)
     |-- region_1: string (nullable = true)
     |-- region_2: string (nullable = true)
     |-- variety: string (nullable = true)
     |-- winery: string (nullable = true)

Then we can use the `show` action to view the data.

```python
df.show(5)
```

    +-------+--------------------+--------------------+------+-----+--------------+-----------------+-----------------+------------------+--------------------+
    |country|         description|         designation|points|price|      province|         region_1|         region_2|           variety|              winery|
    +-------+--------------------+--------------------+------+-----+--------------+-----------------+-----------------+------------------+--------------------+
    |     US|This tremendous 1...|   Martha's Vineyard|    96|  235|    California|      Napa Valley|             Napa|Cabernet Sauvignon|               Heitz|
    |  Spain|Ripe aromas of fi...|Carodorum Selecci...|    96|  110|Northern Spain|             Toro|             null|     Tinta de Toro|Bodega Carmen Rod...|
    |     US|Mac Watson honors...|Special Selected ...|    96|   90|    California|   Knights Valley|           Sonoma|   Sauvignon Blanc|            Macauley|
    |     US|This spent 20 mon...|             Reserve|    96|   65|        Oregon|Willamette Valley|Willamette Valley|        Pinot Noir|               Ponzi|
    | France|This is the top w...|         La Br��lade|    95|   66|      Provence|           Bandol|             null|Provence red blend|Domaine de la B̩gude|
    +-------+--------------------+--------------------+------+-----+--------------+-----------------+-----------------+------------------+--------------------+
    only showing top 5 rows

Transformations are a set of instructions given to Spark to be executed in the future. They are triggered by actions.

Actions perform computations from given transformation instructions.


## Functions

We need to import helper functions from `pyspark` in order to use them in our dataframes. For example, import avg and max.

```python
from pyspark.sql.functions import avg, max

df.select(avg("points")).show()
```

    +-----------------+
    |      avg(points)|
    +-----------------+
    |87.88834105383143|
    +-----------------+

```python
df.select(max("points")).show()
```

    +-----------+
    |max(points)|
    +-----------+
    |         99|
    +-----------+

Order columns.

```python
df.orderBy([df['points'].desc(), df["price"].desc()]).show(5)
```

    +-------+--------------------+--------------------+------+-----+----------+--------------------+------------+------------------+---------------+
    |country|         description|         designation|points|price|  province|            region_1|    region_2|           variety|         winery|
    +-------+--------------------+--------------------+------+-----+----------+--------------------+------------+------------------+---------------+
    |     US|A stupendous Pino...|Precious Mountain...|    99|   94|California|        Sonoma Coast|      Sonoma|        Pinot Noir|Williams Selyem|
    |     US|In a vintage that...|En Chamberlin Vin...|    99|   75|    Oregon|Walla Walla Valle...|Oregon Other|             Syrah|         Cayuse|
    |     US|This is Cabernet ...|            Rockfall|    99|   75|California|    Alexander Valley|      Sonoma|Cabernet Sauvignon|    Stonestreet|
    |     US|This expresses th...|     Hirsch Vineyard|    99|   75|California|        Sonoma Coast|      Sonoma|        Pinot Noir|Williams Selyem|
    |     US|The only one of t...|   Cailloux Vineyard|    99|   65|    Oregon|Walla Walla Valle...|Oregon Other|             Syrah|         Cayuse|
    +-------+--------------------+--------------------+------+-----+----------+--------------------+------------+------------------+---------------+
    only showing top 5 rows


## Filtering

We can filter data with a SQL style.

```python
df.filter("price<20").show(5)
```

    +--------+--------------------+-----------+------+-----+----------+--------------------+-----------------+--------------+--------------------+
    | country|         description|designation|points|price|  province|            region_1|         region_2|       variety|              winery|
    +--------+--------------------+-----------+------+-----+----------+--------------------+-----------------+--------------+--------------------+
    |Bulgaria|This Bulgarian Ma...|    Bergul̩|    90|   15|  Bulgaria|                null|             null|        Mavrud|        Villa Melnik|
    |   Spain|Earthy plum and c...|     Amandi|    90|   17|   Galicia|       Ribeira Sacra|             null|       Menc�_a|      Don Bernardino|
    |      US|There's a lot to ...|       null|    90|   18|California|Russian River Valley|           Sonoma|    Chardonnay|            De Loach|
    |      US|Massively fruity,...|       null|    91|   19|    Oregon|   Willamette Valley|Willamette Valley|    Pinot Gris|   Trinity Vineyards|
    |Portugal|It is the ripe da...|    Premium|    91|   15|  Alentejo|                null|             null|Portuguese Red|Adega Cooperativa...|
    +--------+--------------------+-----------+------+-----+----------+--------------------+-----------------+--------------+--------------------+
    only showing top 5 rows

Filter and get multiple columns.

```python
df.filter("price<20").select(["points", "country", "winery", "price"]).show(5)
```

    +------+--------+--------------------+-----+
    |points| country|              winery|price|
    +------+--------+--------------------+-----+
    |    90|Bulgaria|        Villa Melnik|   15|
    |    90|   Spain|      Don Bernardino|   17|
    |    90|      US|            De Loach|   18|
    |    91|      US|   Trinity Vineyards|   19|
    |    91|Portugal|Adega Cooperativa...|   15|
    +------+--------+--------------------+-----+
    only showing top 5 rows

We can also filter using Python notation.

```python
df.filter((df["price"] > 15) & (df["province"] == "California")).show(5)
```

    +-------+--------------------+--------------------+------+-----+----------+--------------------+--------+------------------+------------+
    |country|         description|         designation|points|price|  province|            region_1|region_2|           variety|      winery|
    +-------+--------------------+--------------------+------+-----+----------+--------------------+--------+------------------+------------+
    |     US|This tremendous 1...|   Martha's Vineyard|    96|  235|California|         Napa Valley|    Napa|Cabernet Sauvignon|       Heitz|
    |     US|Mac Watson honors...|Special Selected ...|    96|   90|California|      Knights Valley|  Sonoma|   Sauvignon Blanc|    Macauley|
    |     US|The producer sour...|Gap's Crown Vineyard|    95|   60|California|        Sonoma Coast|  Sonoma|        Pinot Noir|   Blue Farm|
    |     US|This blockbuster,...|     Rainin Vineyard|    95|  325|California|Diamond Mountain ...|    Napa|Cabernet Sauvignon|        Hall|
    |     US|This fresh and li...|Gap's Crown Vineyard|    95|   75|California|        Sonoma Coast|  Sonoma|        Pinot Noir|Gary Farrell|
    +-------+--------------------+--------------------+------+-----+----------+--------------------+--------+------------------+------------+
    only showing top 5 rows


# Spark Schemas

Because we are dealing with SQL tables, we can create our own schemas.

```python
from pyspark.sql.types import StructField, StringType, IntegerType, StructType

schema = [
    StructField("food", StringType(), True),
    StructField("price", IntegerType(), True)
]
print(schema)
final = StructType(fields=schema)
```

        cls().execute()
      File "/Users/albertovaldez/.pyenv/versions/3.7.13/lib/python3.7/site-packages/mrjob/job.py", line 687, in execute
        self.run_job()
      File "/Users/albertovaldez/.pyenv/versions/3.7.13/lib/python3.7/site-packages/mrjob/job.py", line 634, in run_job
        with self.make_runner() as runner:
      File "/Users/albertovaldez/.pyenv/versions/3.7.13/lib/python3.7/site-packages/mrjob/job.py", line 704, in make_runner
        kwargs = self._runner_kwargs()
      File "/Users/albertovaldez/.pyenv/versions/3.7.13/lib/python3.7/site-packages/mrjob/job.py", line 727, in _runner_kwargs
        self._job_kwargs(),
      File "/Users/albertovaldez/.pyenv/versions/3.7.13/lib/python3.7/site-packages/mrjob/job.py", line 246, in _job_kwargs
        self.libjars(), self.options.libjars),
      File "/Users/albertovaldez/.pyenv/versions/3.7.13/lib/python3.7/site-packages/mrjob/job.py", line 1373, in libjars
        script_dir = os.path.dirname(self.mr_job_script())
      File "/Users/albertovaldez/.pyenv/versions/3.7.13/lib/python3.7/posixpath.py", line 156, in dirname
        p = os.fspath(p)
    TypeError: expected str, bytes or os.PathLike object, not NoneType
    >>> [StructField('food', StringType(), True), StructField('price', IntegerType(), True)]

Then we use said schema to load our data with the correct types.

```python
data = "../resources/food.csv"
spark.sparkContext.addFile(data)
df = spark.read.csv(SparkFiles.get('food.csv'), sep=",", header=True, schema=final)
df.show(5)
```

    

```python
print(type(df['price']))
print(df.dtypes)
```

    <class 'pyspark.sql.column.Column'>
    [('food', 'string'), ('price', 'int')]

We can add a column to our df.

```python
df.withColumn('newprice', df['price']).show()
```

    +-------+-----+--------+
    |   food|price|newprice|
    +-------+-----+--------+
    |  pizza|    0|       0|
    |  sushi|   12|      12|
    |chinese|   10|      10|
    +-------+-----+--------+


# Spark Example 1

Start spark session, get data from url and add to spark session.

```python
from pyspark.sql import SparkSession

spark = SparkSession.builder.appName("Demographics").getOrCreate()
url = "https://2u-data-curriculum-team.s3.amazonaws.com/dataviz-classroom/v1.1/22-big-data/demographics.csv"
spark.sparkContext.addFile(url)
```

    22/10/11 20:11:41 WARN SparkContext: The path https://2u-data-curriculum-team.s3.amazonaws.com/dataviz-classroom/v1.1/22-big-data/demographics.csv has been added already. Overwriting of added paths is not supported in the current version.

Read data from url.

```python
from pyspark import SparkFiles

df = spark.read.csv(SparkFiles.get("demographics.csv"), sep=",", header=True)
df.show(5)
```

    +---+--------------+---+------------+---------+--------+------------------+---------------+------+-------------+
    | id|          name|age|height_meter|weight_kg|children|        occupation|academic_degree|salary|     location|
    +---+--------------+---+------------+---------+--------+------------------+---------------+------+-------------+
    |  0| Darlena Avila| 58|        1.87|       53|       1|     Choreographer|            PhD|    68| South Dakota|
    |  1|      Yan Boyd| 65|         1.8|       40|       0|         Cellarman|       Bachelor|    73|     Delaware|
    |  2|   Joette Lane| 32|         1.8|       73|       1|Veterinary Surgeon|         Master|    69| South Dakota|
    |  3|  Jazmine Hunt| 61|        1.79|       89|       0|            Hawker|            PhD|    88|    Louisiana|
    |  4|Remedios Gomez| 23|        1.64|       51|       2|     Choreographer|       Bachelor|    83|West Virginia|
    +---+--------------+---+------------+---------+--------+------------------+---------------+------+-------------+
    only showing top 5 rows

Print the column names.

```python
print(df.columns)
```

    ['id', 'name', 'age', 'height_meter', 'weight_kg', 'children', 'occupation', 'academic_degree', 'salary', 'location']

See the first 10 rows.

```python
df.show(10)
```

    +---+--------------------+---+------------+---------+--------+------------------+---------------+------+-------------+
    | id|                name|age|height_meter|weight_kg|children|        occupation|academic_degree|salary|     location|
    +---+--------------------+---+------------+---------+--------+------------------+---------------+------+-------------+
    |  0|       Darlena Avila| 58|        1.87|       53|       1|     Choreographer|            PhD|    68| South Dakota|
    |  1|            Yan Boyd| 65|         1.8|       40|       0|         Cellarman|       Bachelor|    73|     Delaware|
    |  2|         Joette Lane| 32|         1.8|       73|       1|Veterinary Surgeon|         Master|    69| South Dakota|
    |  3|        Jazmine Hunt| 61|        1.79|       89|       0|            Hawker|            PhD|    88|    Louisiana|
    |  4|      Remedios Gomez| 23|        1.64|       51|       2|     Choreographer|       Bachelor|    83|West Virginia|
    |  5|        Myung Brewer| 20|        1.68|       60|       4|    Window Dresser|       Bachelor|    65| South Dakota|
    |  6|         Shaun Lynch| 31|        1.56|       62|       0|            Weaver|         Master|    72|    Louisiana|
    |  7|     Melodi Mcdowell| 56|         1.6|       42|       0| Lighthouse Keeper|         Master|    65|    Louisiana|
    |  8|Charlesetta Steve...| 30|        1.62|       44|       3|        Millwright|         Master|    87|    Louisiana|
    |  9|       Merri Charles| 44|        1.69|       51|       5|  Medical Supplier|            PhD|    72|West Virginia|
    +---+--------------------+---+------------+---------+--------+------------------+---------------+------+-------------+
    only showing top 10 rows

Get the age, height\_meter, and weight\_kg columns and describe them.

```python
df.select(["age", "height_meter", "weight_kg"]).describe().show()
```

    +-------+------------------+------------------+------------------+
    |summary|               age|      height_meter|         weight_kg|
    +-------+------------------+------------------+------------------+
    |  count|              1000|              1000|              1000|
    |   mean|            42.933|1.7519499999999995|            64.011|
    | stddev|14.255445581556843|0.1436897499623555|15.005733939099779|
    |    min|                18|               1.5|                38|
    |    max|                67|                 2|                90|
    +-------+------------------+------------------+------------------+

Print the schema of the df.

```python
df.printSchema()
```

    root
     |-- id: string (nullable = true)
     |-- name: string (nullable = true)
     |-- age: string (nullable = true)
     |-- height_meter: string (nullable = true)
     |-- weight_kg: string (nullable = true)
     |-- children: string (nullable = true)
     |-- occupation: string (nullable = true)
     |-- academic_degree: string (nullable = true)
     |-- salary: string (nullable = true)
     |-- location: string (nullable = true)

Rename the Salary column.

```python
df = df.withColumnRenamed('Salary', 'Salary (1k)')
df[['Salary (1k)']].show(5)
```

    +-----------+
    |Salary (1k)|
    +-----------+
    |         68|
    |         73|
    |         69|
    |         88|
    |         83|
    +-----------+
    only showing top 5 rows

Create new column named Salary.

```python
df = df.withColumn("Salary", df["Salary (1k)"]*1000)
df[['Salary', 'Salary (1k)']].show(5)
```

    +-------+-----------+
    | Salary|Salary (1k)|
    +-------+-----------+
    |68000.0|         68|
    |73000.0|         73|
    |69000.0|         69|
    |88000.0|         88|
    |83000.0|         83|
    +-------+-----------+
    only showing top 5 rows


# Spark Example 2

```python
from pyspark.sql import SparkSession
from pyspark import SparkFiles

spark = SparkSession.builder.appName("demographicsFilter").getOrCreate()
url = "https://2u-data-curriculum-team.s3.amazonaws.com/dataviz-classroom/v1.1/22-big-data/demographics.csv"
spark.sparkContext.addFile(url)
```

```python
df = spark.read.option('header', 'true').csv(SparkFiles.get("demographics.csv"), inferSchema=True, sep=',')
df.show(5)
```

    +---+--------------+---+------------+---------+--------+------------------+---------------+------+-------------+
    | id|          name|age|height_meter|weight_kg|children|        occupation|academic_degree|salary|     location|
    +---+--------------+---+------------+---------+--------+------------------+---------------+------+-------------+
    |  0| Darlena Avila| 58|        1.87|       53|       1|     Choreographer|            PhD|    68| South Dakota|
    |  1|      Yan Boyd| 65|         1.8|       40|       0|         Cellarman|       Bachelor|    73|     Delaware|
    |  2|   Joette Lane| 32|         1.8|       73|       1|Veterinary Surgeon|         Master|    69| South Dakota|
    |  3|  Jazmine Hunt| 61|        1.79|       89|       0|            Hawker|            PhD|    88|    Louisiana|
    |  4|Remedios Gomez| 23|        1.64|       51|       2|     Choreographer|       Bachelor|    83|West Virginia|
    +---+--------------+---+------------+---------+--------+------------------+---------------+------+-------------+
    only showing top 5 rows

What occupation had the highest salary?

```python
df.orderBy(df['salary'].desc()).select(df["occupation"]).show(1)
```

    +-----------------+
    |       occupation|
    +-----------------+
    |Medical Physicist|
    +-----------------+
    only showing top 1 row

What occupation had the lowest salary?

```python
df.orderBy(df['salary'].asc()).select(df["occupation"]).show(1)
```

    +-----------------+
    |       occupation|
    +-----------------+
    |Lighthouse Keeper|
    +-----------------+
    only showing top 1 row

What is the mean salary of this dataset? What is the max and min of the Salary column?

```python
from pyspark.sql.functions import avg, min, max

salary_avg = df.select(avg("salary")).collect()
salary_min = df.select(min("salary")).collect()
salary_max = df.select(max("salary")).collect()
print(salary_avg, salary_min, salary_max)
```

    [Row(avg(salary)=77.738)] [Row(min(salary)=65)] [Row(max(salary)=90)]

Show all of the occupations where salaries were above 80k

```python
df.filter(df["salary"] > 80).select(df["occupation"]).show(5)
```

    +-----------------+
    |       occupation|
    +-----------------+
    |           Hawker|
    |    Choreographer|
    |       Millwright|
    |Medical Physicist|
    |        Scientist|
    +-----------------+
    only showing top 5 rows

What is the average age and height for each academic degree type?

```python
group = df.groupBy("academic_degree").avg()
group.select('academic_degree', "avg(age)", "avg(height_meter)").show()
```

    +---------------+------------------+------------------+
    |academic_degree|          avg(age)| avg(height_meter)|
    +---------------+------------------+------------------+
    |            PhD| 43.15976331360947|1.7438165680473379|
    |         Master|43.139318885448915|1.7549226006191951|
    |       Bachelor| 42.51032448377581| 1.757227138643069|
    +---------------+------------------+------------------+


# Working with Dates

InferSchema is performance heavy, it tries to guess the data types for the schema. TimestampFormat contains the reading format of the dates.

We will import time functions from the sql, functions module to work with dates.

```python
from pyspark import SparkFiles
from pyspark.sql.functions import year, month

url = "../resources/bigfoot.csv"
spark.sparkContext.addFile(url)
df = spark.read.csv(SparkFiles.get("bigfoot.csv"), header=True, inferSchema=True, timestampFormat="yyyy-MM-dd HH:mm:ss")

df.printSchema()
```

    22/10/11 21:02:23 WARN SparkContext: The path ../resources/bigfoot.csv has been added already. Overwriting of added paths is not supported in the current version.
    root
     |-- number: integer (nullable = true)
     |-- title: string (nullable = true)
     |-- classification: string (nullable = true)
     |-- timestamp: string (nullable = true)
     |-- latitude: string (nullable = true)
     |-- longitude: string (nullable = true)

Convert data to year, month.

```python
df.select(year(df['timestamp']), month(df['timestamp'])).show(5)
```

    +---------------+----------------+
    |year(timestamp)|month(timestamp)|
    +---------------+----------------+
    |           2000|               6|
    |           1995|               5|
    |           2004|               2|
    |           2004|               6|
    |           2004|               2|
    +---------------+----------------+
    only showing top 5 rows

Creating new columns with year data.

```python
df = df.withColumn('year', year(df['timestamp']))
df = df.withColumn('month', month(df['timestamp']))
df.show(5)
```

    +------+--------------------+--------------+--------------------+--------+---------+----+-----+
    |number|               title|classification|           timestamp|latitude|longitude|year|month|
    +------+--------------------+--------------+--------------------+--------+---------+----+-----+
    |   637|Report 637: Campe...|       Class A|2000-06-16T12:00:00Z|    61.5|   -142.9|2000|    6|
    |  2917|Report 2917: Fami...|       Class A|1995-05-15T12:00:00Z| 55.1872|-132.7982|1995|    5|
    |  7963|Report 7963: Sasq...|       Class A|2004-02-09T12:00:00Z| 55.2035|-132.8202|2004|    2|
    |  9317|Report 9317: Driv...|       Class A|2004-06-18T12:00:00Z| 62.9375|-141.5667|2004|    6|
    | 13038|Report 13038: Sno...|       Class A|2004-02-15T12:00:00Z| 61.0595|-149.7853|2004|    2|
    +------+--------------------+--------------+--------------------+--------+---------+----+-----+
    only showing top 5 rows

Grouping averages by year.

```python
by_year = df.groupBy('year').avg()
by_year.orderBy(df['year'].desc()).select('year', 'avg(number)').show(5)
```

    +----+------------------+
    |year|       avg(number)|
    +----+------------------+
    |2053|           45647.0|
    |2017| 51822.11111111111|
    |2016| 53143.73529411765|
    |2015|49102.101694915254|
    |2014|  46456.8275862069|
    +----+------------------+
    only showing top 5 rows


# Plotting Data

TODO: Finish plotting data section.

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://hadoop.apache.org/>
